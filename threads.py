#!/usr/bin/python
import math
import threading
import time
import socket as sock

import packets
from adam_optimizer import AdamOptimizer
from read_temp import read_temp_celsius

lock = threading.Lock()

HOUR = 3600.0
MAX_CONNECTIONS = 10


class ReceiveThread(threading.Thread):
    def __init__(self, server):
        super(ReceiveThread, self).__init__()

        self.server = server
        self.socket = None

    def run(self):
        self.socket = sock.socket(sock.AF_INET, sock.SOCK_STREAM)
        self.socket.bind(("0.0.0.0", 8192))
        self.socket.listen(MAX_CONNECTIONS)

        while True:
            connection, address = self.socket.accept()
            print("Connected to {}".format(address))
            thread = threading.Thread(target=self.client_thread, args=(connection,))
            thread.start()

    def client_thread(self, connection):
        while True:
            received_data = connection.recv(1024)
            print("Received: {}".format(received_data))

            # Client closed connection
            if not received_data:
                if connection in self.server.connections:
                    self.server.connections.remove(connection)
                break

            try:
                packet = packets.parse_packet(received_data)
            except ValueError:
                continue  # No id for packet: skip the data

            packet.on_receive(self.server, connection)

        connection.close()


class ReadTempThread(threading.Thread):
    def __init__(self, server):
        super(ReadTempThread, self).__init__()
        self.server = server

    def run(self):
        while True:
            time.sleep(5)

            with lock:
                time_since_start = self.server.time_since_start()
                temp = read_temp_celsius()

                normalized_time = time_since_start / HOUR
                normalized_temp = temp / self.server.data.temp_for_normalizing

                self.server.data.measurements.append([time_since_start, temp])
                self.server.data.normalized_measurements.append([normalized_time, normalized_temp])

                time_until_desired_temp_seconds = self.server.time_until_desired_temperature()
                time_until_desired_temp_millis = time_until_desired_temp_seconds * 1000
                packet = packets.NewDataPacket().with_data(self.server.data, time_until_desired_temp_millis)
                self.server.send_packet_to_all(packet)

                if (self.server.data.alarm_at >= time_until_desired_temp_millis > 0) and not self.server.has_alarm_gone:
                    self.server.has_alarm_gone = True
                    packet = packets.AlarmPacket()
                    self.server.send_packet_to_all(packet)


class RegressionThread(threading.Thread):
    def __init__(self, server):
        super(RegressionThread, self).__init__()
        self.server = server
        data = server.data
        self.optimizer = AdamOptimizer(
            lambda x: (data.a - data.b) / (1.0 + math.exp((x - data.c) / (x * data.d))) + data.b)

    def run(self):
        while True:
            with lock:
                data = self.server.data
                value_changes = self.optimizer.minimize(
                    [
                        lambda x: 1.0 / (math.exp((x - data.c) / (data.d * x)) + 1.0),
                        lambda x: 1 - (1 / (math.exp((x - data.c) / (data.d * x)) + 1)),
                        lambda x: ((data.a - data.b) * math.exp((x - data.c) / (data.d * x))) / (
                                data.d * x * (math.exp((x - data.c) / (data.d * x)) + 1) ** 2),
                        lambda x: ((data.a - data.b) * (x - data.c) * math.exp((x - data.c) / (data.d * x))) / (
                                data.d * data.d * x * (math.exp((x - data.c) / (data.d * x)) + 1) ** 2)
                    ], data.normalized_measurements
                )

                data.a += value_changes[0]
                data.b += value_changes[1]
                data.c += value_changes[2]
                data.d += value_changes[3]
