#!/usr/bin/python
import RPi.GPIO as GPIO
import read_adc
import time
import csv

# Constants
V_REF = 5.0
R1 = 235e3
MIN_VOLTAGE = 1e-5


def read_ntc():
    voltage = read_adc.read_voltage(V_REF)
    # Make sure voltage is never 0 to prevent division by zero
    voltage = max(MIN_VOLTAGE, voltage)

    # Use this one when ntc is above the other resistor
    # resistance = R1 * ((V_REF / voltage) - 1)
    resistance = R1 / (V_REF * (1 / voltage) - 1)
    return resistance


def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    read_adc.setup(read_adc.CS, read_adc.MOSI, read_adc.MISO, read_adc.CLK)
