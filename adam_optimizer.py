#!/usr/bin/python
import math

epsilon = 1e-8
beta1 = 0.9
beta2 = 0.999


class AdamOptimizer:

    def __init__(self, f, learning_rate=0.001):
        self.f = f

        self.learning_rate = learning_rate

        self.previous_beta1 = beta1
        self.previous_beta2 = beta2

        self.previous_first_moment = {}
        self.previous_second_moment = {}

    def minimize(self, partial_derivatives, data):
        gradients = self.compute_gradients(partial_derivatives, data)
        value_changes = self.apply_gradients(gradients)
        return value_changes

    def compute_gradients(self, partial_derivatives, data):
        gradients = [0] * len(partial_derivatives)

        N = float(len(data))
        for data_point in data:
            x = data_point[0]
            y = data_point[1]

            error = self.f(x) - y

            for i in range(len(partial_derivatives)):
                gradients[i] += (1 / N) * error * partial_derivatives[i](x)

        return gradients

    def apply_gradients(self, gradients):
        value_changes = []

        for i in range(len(gradients)):
            gradient = gradients[i]

            if i not in self.previous_first_moment:
                self.previous_first_moment[i] = 0
            if i not in self.previous_second_moment:
                self.previous_second_moment[i] = 0

            previous_first_moment = self.previous_first_moment[i]
            previous_second_moment = self.previous_second_moment[i]

            new_first_moment = beta1 * previous_first_moment + (1 - beta1) * gradient
            new_second_moment = beta2 * previous_second_moment + (1 - beta2) * gradient ** 2

            bias_corrected_first_moment = new_first_moment / (1 - self.previous_beta1)
            bias_corrected_second_moment = new_second_moment / (1 - self.previous_beta2)

            value_changes.append(-self.learning_rate * bias_corrected_first_moment / (
                    math.sqrt(bias_corrected_second_moment) + epsilon))

            self.previous_first_moment[i] = new_first_moment
            self.previous_second_moment[i] = new_second_moment

        self.previous_beta1 *= beta1
        self.previous_beta2 *= beta2

        return value_changes
