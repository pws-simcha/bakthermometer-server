#!/usr/bin/python
import json
import os
import signal
import sys


class PacketType:
    NEW_DATA = 0
    CHANGED_DESIRED_TEMP = 1
    CHANGED_ALARM_AT = 2
    NO_MEASUREMENT = 3
    MEASUREMENT_STOPPED = 4
    JOINED_MEASUREMENT = 5
    STARTED_MEASUREMENT = 6
    ALARM = 7

    START_MEASUREMENT = 8
    CHANGE_DESIRED_TEMP = 9
    CHANGE_ALARM_AT = 10
    JOIN_MEASUREMENT = 11
    STOP_MEASUREMENT = 12
    LEAVE_MEASUREMENT = 13


def parse_packet(json_string):
    json_object = json.loads(json_string)

    try:
        packet_id = json_object["id"]
    except KeyError:
        packet_id = -1

    if packet_id in packet_types:
        packet = packet_types[packet_id]()
    else:
        raise ValueError("There is no packet with id {}".format(packet_id))

    packet.__dict__.update(json_object)

    return packet


class Packet:
    def __init__(self, packet_id):
        self.id = packet_id

    def to_json(self):
        return json.dumps(self.__dict__)

    def on_receive(self, server, connection):
        pass


"""
Server side
"""


class NewDataPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.NEW_DATA)

        self.measurement = None
        self.time_until_desired_temp = None

    def with_data(self, data, time_until_desired_temp):
        self.measurement = data.measurements[len(data.measurements) - 1]
        self.time_until_desired_temp = time_until_desired_temp
        return self


class ChangedDesiredTempPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.CHANGED_DESIRED_TEMP)

        self.desired_temp = None

    def with_desired_temp(self, new_desired_temp):
        self.desired_temp = new_desired_temp
        return self


class ChangedAlarmAtPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.CHANGED_ALARM_AT)

        self.alarm_at = None

    def with_alarm_at(self, alarm_at):
        self.alarm_at = alarm_at
        return self


class NoMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.NO_MEASUREMENT)


class MeasurementStoppedPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.MEASUREMENT_STOPPED)


class JoinedMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.JOINED_MEASUREMENT)

        self.desired_temp = None
        self.alarm_at = None

    def with_data(self, desired_temp, alarm_at):
        self.desired_temp = desired_temp
        self.alarm_at = alarm_at
        return self


class StartedMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.STARTED_MEASUREMENT)

        self.desired_temp = None
        self.alarm_at = None

    def with_data(self, desired_temp, alarm_at):
        self.desired_temp = desired_temp
        self.alarm_at = alarm_at
        return self


class AlarmPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.ALARM)


"""
Client side
"""


class StartMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.START_MEASUREMENT)

        self.desired_temp = None
        self.alarm_at = None

    def on_receive(self, server, connection):
        if not server.running_measurement:
            server.start_measurement(self.desired_temp, self.alarm_at)
            server.connections.add(connection)

            packet = StartedMeasurementPacket().with_data(self.desired_temp, self.alarm_at)
            server.send_packet(connection, packet)


class ChangeDesiredTempPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.CHANGE_DESIRED_TEMP)

        self.desired_temp = None

    def on_receive(self, server, connection):
        server.data.desired_temp = self.desired_temp

        packet = ChangedDesiredTempPacket().with_desired_temp(self.desired_temp)
        server.send_packet_to_all(packet)


class ChangeAlarmAtPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.CHANGE_ALARM_AT)

        self.alarm_at = None

    def on_receive(self, server, connection):
        server.data.alarm_at = self.alarm_at

        packet = ChangedAlarmAtPacket().with_alarm_at(self.alarm_at)
        server.send_packet_to_all(packet)


class JoinMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.JOIN_MEASUREMENT)

    def on_receive(self, server, connection):
        if server.running_measurement:
            server.connections.add(connection)

            packet = JoinedMeasurementPacket().with_data(server.data.desired_temp, server.data.alarm_at)
            server.send_packet(connection, packet)
        else:
            server.send_packet(connection, NoMeasurementPacket())


class StopMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.STOP_MEASUREMENT)

    def on_receive(self, server, connection):
        packet = MeasurementStoppedPacket()
        server.send_packet_to_all(packet)
        server.running_measurement = False

        for connection in server.connections:
            connection.close()

        server.receive_thread.socket.close()

        kill_parent()
        sys.exit(0)


def kill_parent():
    r = os.fork()
    if r is not 0:
        kill_child()


def kill_child():
    os.kill(os.getpid(), signal.SIGKILL)


class LeaveMeasurementPacket(Packet):
    def __init__(self):
        Packet.__init__(self, PacketType.LEAVE_MEASUREMENT)

    def on_receive(self, server, connection):
        server.connections.remove(connection)
        connection.close()


packet_types = {
    PacketType.NEW_DATA: NewDataPacket,
    PacketType.CHANGED_DESIRED_TEMP: ChangedDesiredTempPacket,
    PacketType.CHANGED_ALARM_AT: ChangedAlarmAtPacket,
    PacketType.NO_MEASUREMENT: NoMeasurementPacket,
    PacketType.MEASUREMENT_STOPPED: MeasurementStoppedPacket,
    PacketType.JOINED_MEASUREMENT: JoinedMeasurementPacket,
    PacketType.STARTED_MEASUREMENT: StartedMeasurementPacket,

    PacketType.ALARM: AlarmPacket,
    PacketType.START_MEASUREMENT: StartMeasurementPacket,
    PacketType.CHANGE_DESIRED_TEMP: ChangeDesiredTempPacket,
    PacketType.CHANGE_ALARM_AT: ChangeAlarmAtPacket,
    PacketType.JOIN_MEASUREMENT: JoinMeasurementPacket,
    PacketType.STOP_MEASUREMENT: StopMeasurementPacket,
    PacketType.LEAVE_MEASUREMENT: LeaveMeasurementPacket,
}
