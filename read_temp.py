#!/usr/bin/python
import read_ntc
import math

# Constants
A = 1.24968388226e-3
B = 1.18838653114e-4
C = 3.58209359044e-7


def setup():
    read_ntc.setup()


def read_temp_celsius():
    return read_temp_kelvin() - 273.15


def read_temp_kelvin():
    resistance = read_ntc.read_ntc()
    return 1.0 / (A + B * math.log(resistance) + C * (math.log(resistance) ** 3))
