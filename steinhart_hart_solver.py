#!/usr/bin/python
import numpy as np
import math


def solve(resistances, temperatures):
    if type(resistances) is not list or type(temperatures) is not list:
        raise ValueError("Both resistances and temperatures need to be a list")
    if len(resistances) is not 3 or len(temperatures) is not 3:
        raise ValueError("Both resistances and temperatures only can hold 3 values")
    if temperatures[0] < 200:
        raise ValueError("You probably didn't enter the temperatures in Kelvin")

    # Fill the temperatures list with 1/temperature
    temperatures = map(lambda temperature: 1 / float(temperature), temperatures)
    # Resistances is now a two-dimensional list
    resistances = map(lambda resistance: [1, math.log(resistance), math.log(resistance) ** 3], resistances)

    resistances_inverse = np.linalg.inv(resistances)
    """
    If you multiply resistances with it's inverse, you end up with the identity matrix.
    Multiplying the identity matrix with [a, b, c] will have [a, b, c] as result since
    multiplying with the identity matrix, is the same as multiplying with 1 in normal algerbra.
    So if we multiply both sides with the inverse of resistances we end up with the following equation:
    [a, b, c] = resistances_inverse * temperatures.
    """
    a, b, c = np.dot(resistances_inverse, temperatures)
    return a, b, c


if __name__ == "__main__":
    coefficients = solve([550000, 210000, 13000], [274.15, 297.15, 373.15])
    print("a: {}\nb: {}\nc: {}".format(coefficients[0], coefficients[1], coefficients[2]))
