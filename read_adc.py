#!/usr/bin/python
import RPi.GPIO as GPIO

# Constants
RESOLUTION = 2 ** 10

# Pins numbers
CS = 3
MOSI = 17
MISO = 22
CLK = 2


def setup(cs, mosi, miso, clk):
    GPIO.setup(cs, GPIO.OUT)
    GPIO.setup(mosi, GPIO.OUT)
    GPIO.setup(miso, GPIO.IN)
    GPIO.setup(clk, GPIO.OUT)


def read_voltage(reference_voltage):
    data = read_adc()
    # We make a float of it so the division works
    voltage = data / float(RESOLUTION - 1) * reference_voltage
    return voltage


def read_adc(channel=0):
    if channel not in range(2):
        return -1

    # Start communication
    GPIO.output(CS, GPIO.HIGH)
    GPIO.output(CLK, GPIO.LOW)
    GPIO.output(CS, GPIO.LOW)

    # Channel is now 0b00 or 0b10
    channel <<= 1

    command = 0b1101
    command |= channel

    # Send data
    mask = 0b1000
    for i in range(4):
        bit = command & mask
        mask >>= 1

        GPIO.output(MOSI, bit)
        GPIO.output(CLK, GPIO.HIGH)
        GPIO.output(CLK, GPIO.LOW)

    # Receiving data
    data = 0
    for i in range(11):
        GPIO.output(CLK, GPIO.HIGH)
        GPIO.output(CLK, GPIO.LOW)

        data <<= 1

        bit = GPIO.input(MISO)
        data |= bit

    # Shift one bit back, to get rid of the null bit
    data >>= 1

    # We received everything. CS must be high again
    GPIO.output(CS, GPIO.HIGH)

    return data
