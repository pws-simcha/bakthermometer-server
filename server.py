#!/usr/bin/python
import time

from data import Data
from threads import ReadTempThread, RegressionThread, ReceiveThread

HOUR = 3600.0


class Server:
    def __init__(self):
        self.data = None  # Will be initialized after a packet
        self.connections = set()  # Use a set to prevent duplicates
        self.running_measurement = False
        self.has_alarm_gone = False
        self.start_time = None

        self.receive_thread = ReceiveThread(self)
        self.receive_thread.start()

    def start_measurement(self, desired_temp, alarm_at):
        self.running_measurement = True
        self.data = Data(desired_temp, alarm_at)
        self.start_time = time.time()

        read_temp_thread = ReadTempThread(self)
        regression_thread = RegressionThread(self)

        read_temp_thread.start()
        regression_thread.start()

    def time_since_start(self):
        return time.time() - self.start_time

    def time_until_desired_temperature(self):
        time_at_desired_temperature = self.data.time_at_desired_temperature()
        # No valid prediction yet
        if time_at_desired_temperature <= 0:
            return -1

        return time_at_desired_temperature - self.time_since_start()

    def send_packet_to_all(self, packet):
        for connection in self.connections:
            self.send_packet(connection, packet)

    def send_packet(self, connection, packet):
        print("Sent: {}".format(packet.to_json()))
        connection.sendall(packet.to_json() + "\n")
