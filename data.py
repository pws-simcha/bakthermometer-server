#!/usr/bin/python
import math

HOUR = 3600.0


class Data:
    def __init__(self, desired_temp, alarm_at):
        self.temp_for_normalizing = desired_temp

        self.desired_temp = desired_temp
        self.alarm_at = alarm_at

        self.a = 0.3
        self.b = 1.0
        self.c = 0.5
        self.d = 0.5

        self.measurements = []
        self.normalized_measurements = []

    def time_at_desired_temperature(self):
        normalized_temp = self.desired_temp / self.temp_for_normalizing

        if self.is_close(normalized_temp, self.b):
            return -1
        if (self.a - self.b) / (normalized_temp - self.b) <= 1:
            return -1

        normalized_time = self.c / (1.0 - self.d * math.log((self.a - self.b) / (normalized_temp - self.b) - 1))
        return normalized_time * HOUR

    def is_close(self, a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
